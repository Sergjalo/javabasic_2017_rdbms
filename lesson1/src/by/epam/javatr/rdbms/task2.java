package by.epam.javatr.rdbms;
import java.util.Scanner;

/**
 *  ��������� ����� ���������� � ������� ����� ������ � ���� �� ��������� ������� R
 *  @author Sergii Kotov
 */
public class task2 {
	public static void main(String[] args) {
		// 2*pi*r 
		// pi*r*r
		@SuppressWarnings("resource")
		double r=0;
		Scanner scanner = new Scanner(System.in);
		System.out.print("������� �������� �������: ");
		if(scanner.hasNextDouble()) {
			r = scanner.nextDouble();
		}else{
		scanner.next();
		System.out.println("�� ����� ������������ �����");
		}
		
		System.out.println("����� ���������� ��� ������� "+r+" ���������� "+circleLine(r));
		System.out.println("����� ���������� ��� ������� "+r+" ���������� "+circleSq(r));
	}
	
	static private double circleLine(double r) {
		return r*2*Math.PI;
	}

	static private double circleSq(double r) {
		return r*r*Math.PI;
	}
}